
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
using namespace std;

class Mystring
{
private:
    char *tab; //le support tableau n'est plus un imp�ratif -> a la fin du tableau il y a un \0 (plus impeartif car 2 eme attribut ----> si on change tab on doit mettre a jour tout le reste genre n,spe et stat
    int n, spe;// n represente le delimiteur de fin de chaine et le nombre de caractere dans le tableau et spe est un compteur de caractere speciaux pas dans {a,z}
    int * stat;// nb d'apparition d'une lettre de l'alphabet (minuscule ou majuscule)
    void maj(); // pour mettre a jour n , spe , stat ---->l'algo de l'occurence stat

public:
    Mystring(); //methode sans argument pour un tableau vide (le pointeur est mis a NULL)
    Mystring(char*); // methode avec un argument pour initialiser le tableau
    Mystring(int,char); // methode avec 2 arguments pour dupliquer une lettre

    ~Mystring();

    Mystring(const Mystring&);
    void affiche();
    void supprime(char);
    void dedouble(char);
    void concat (Mystring);
     //accesseurs des GET et des SET
};

Mystring::Mystring()
{
    tab=NULL; // on peut mettre aussi nullptr
    n=spe=0;
    stat= new int[26];
    for (int i=0;i<26;i++)
    {
       stat[i]=0;
    }
    cout<<"constructeur 1"<<endl;
}
Mystring::Mystring(char* pch)
{
    n=strlen(pch); // comptage donc on peut faire strlen ,qui lui permet � l'utilisateur de rien faire du tout.
    tab= new char [n+1];
    //copie du flux source vers le flux destination donc on met notre fonction de copie avec strlencpy
    strcpy(tab,pch);
    spe=0;
    stat= new int [26];
    maj();
}

Mystring::Mystring(int nbchar,char carac)
{
    n=nbchar;
    tab = new char [n];
    for(int i=0;i<nbchar;i++)
    {
        tab[i]=carac;
    }
    spe=0;
    stat= new int [26];
    maj();
}

Mystring::Mystring(const Mystring&s)
{
    cout<<"Clonage dynamique de deux chaines"<<endl;
   n=s.n;
   spe=s.spe;
   tab=new char[n+1];
   strcpy(tab,s.tab);
   stat=new int[26];
   for(int i=0;i<26;i++)
    stat[i]=s.stat[i];
   cout<<"Fin de clonage dynamique de deux chaines"<<endl;
}

Mystring::~Mystring()
{
    delete tab;
    delete stat;
    cout<<"Je detruit presque tout"<<endl; // Le destructeur de l'objet instanci� par la matrice d'instanciation instanci�e elle-m�me par le gen�rateur d'instanciation instancielles qui instancie les instances instancielles de l'instantiation d'instantivit� instanci�e.
}

void Mystring:: maj()
{
    spe=0;
    for(int i=0;i<26;i++)
    {
        stat[i]=0;
    }

   int x = 0, i, j, k, repete = 0;

char Alpha[] = { 'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
char caractere;

for (i=0;i<26;i++)
{
    stat[i]=0;
}

  for (i=0;tab[i]!='\0';i++)
  {
      int cpt=0;
      for(j=0;j<26;j++)
      {
        caractere=tolower(tab[i]); //Convertir MAJUSCULE en minuscule
          if(caractere==Alpha[j])
          {
              stat[j]=stat[j]+1;
              cpt=1;
          }
      }
      if(cpt==0)
      {
          spe++;
      }
  }
    n=i; // pour mettre n a jour
    // on l'a c'te fonction la
}

void Mystring::affiche()
{
    for(int i=0;i<n;i++)
    {
        cout<<tab[i];
    }
    cout<<endl;

    for(int i=0;i<26;i++)
{
    printf("%c est %d fois \n",i+'a',stat[i]);
}

    printf("il y a %d carac speciaux \n",spe);
}


int main()
{
    Mystring S1;
    S1.affiche(); // chaine =0 , n=0, spe=0, stat=.......
    Mystring S2("chaine"); // on peut aussi faire comme �a: char tmp []="abcbaed"; Mystring S2(tmp)
    S2.affiche(); // chaine=abcbaed n=5,  spe=0, il ya 2 a etc .......
    Mystring S3 (4,'b');
    S3.affiche(); // chaine =bbbb n=4 spe=0, il y a 4 b
    Mystring S4 ("ChaineSp�ciale12^^");
    S4.affiche();
    Mystring S5;
    S5=S2;
    S5.affiche();
}
